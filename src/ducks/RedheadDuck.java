package ducks;

public class RedheadDuck extends Duck{
    @Override
    public String display() {
        return "Showing a redhead duck!";
    }
}

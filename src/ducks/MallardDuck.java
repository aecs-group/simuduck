package ducks;

public class MallardDuck extends Duck{
    @Override
    public String display() {
        return "Showing a Mallard duck";
    }
    public String displayMyType(){
        return "My type is Mallard";
    }
}

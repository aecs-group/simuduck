package ducks;

public class Duck {
    public String quack(){
        return "Quacking!";
    }
    public String swim(){
        return "Swiming!";
    }
    public String display(){
        return "Showing Father Duck";
    }
}

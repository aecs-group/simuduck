import ducks.Duck;
import ducks.MallardDuck;
import ducks.RedheadDuck;

public class Main {
    public static void main(String[] args) {

        System.out.println("********** Initializing Duck Apps **********");
        /* Create 3 ducks*/
        Duck fatherDuck = new Duck();
        RedheadDuck redHeadDuck = new RedheadDuck();
        MallardDuck mallardDuck = new MallardDuck();
        /* Using their methods*/
        System.out.println(fatherDuck.display());
        System.out.println(redHeadDuck.display());
        System.out.println(mallardDuck.display());

    }
}